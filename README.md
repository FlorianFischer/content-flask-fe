This is a sideproject of Florian Fischer. Please note this is a work in progress.
Currently implemented is token based authentication including signup, login, and password reset.

The project is deployed to AWS and can be accessed at https://d2p4q7h8vxe1l6.cloudfront.net/

Please note that you will have to allow insecure access in Chrome and Firefox since there is no SSL certificate for the API yet.
