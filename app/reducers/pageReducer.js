import * as types from '../actions/types';
import { insertAt, shiftItem, removeItem } from '../utils/utils';


const initialState = {
  byId: {}
};


const pageReducer = (state = initialState, action) => {
  switch(action.type) {
    case types.CREATE_PROJECT_SUCCESS:
    case types.SAVE_PROJECT_SUCCESS:
    case types.FETCH_PROJECT_SUCCESS:
    case types.FETCH_PROJECTS_SUCCESS:
      return {
        ...state,
        byId: {
          ...state.byId,
          ...action.payload.pages
        }
      };

    case types.ADD_PAGE:
      const { page } = action.payload;
      return {
        ...state,
        byId: {
          ...state.byId,
          [page._tempId]: page
        }
      };

    case types.UPDATE_PAGE:
      const { id, values } = action.payload;
      return {
        ...state,
        byId: {
          ...state.byId,
          [id]: {
            ...state.byId[id],
            ...values
          }
        }
      };

    case types.REMOVE_PAGE:
    case types.DELETE_PAGE_SUCCESS: {
      const { pageId } = action.payload;
      const newById = { ...state.byId };
      delete newById[pageId];

      return {
        ...state,
        byId: { ...newById }
      };
    }
    case types.ADD_SECTION: {
      const { pageId, index } = action.payload;

      return {
        ...state,
        byId: {
          ...state.byId,
          [pageId]: {
            ...state.byId[pageId],
            sections: insertAt(state.byId[pageId].sections, action.payload.section._tempId, index)
          }
        }
      };
    }
    case types.REORDER_SECTION: {
      const { pageId, sectionId, index } = action.payload;

      return {
        ...state,
        byId: {
          ...state.byId,
          [pageId]: {
            ...state.byId[pageId],
            sections: shiftItem(state.byId[pageId].sections, sectionId, index)
          }
        }
      };
    }
    case types.REMOVE_SECTION: {
      const { pageId, sectionId } = action.payload;
      return {
        ...state,
        byId: {
          ...state.byId,
          [pageId]: {
            ...state.byId[pageId],
            sections: removeItem(state.byId[pageId].sections, sectionId)
          }
        }
      };
    }
    default:
      return state;
  }
};


export default pageReducer;
