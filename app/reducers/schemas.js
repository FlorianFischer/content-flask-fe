import { schema, arrayOf } from 'normalizr';


export const projectSchema = new schema.Entity('projects', {}, { idAttribute: '_id' });
export const pageSchema = new schema.Entity('pages', {}, { idAttribute: '_id' });
export const sectionSchema = new schema.Entity('sections', {}, { idAttribute: '_id' });
export const projectListSchema = new schema.Array(projectSchema);


projectSchema.define({
  pages: [pageSchema]
});


pageSchema.define({
  sections: [sectionSchema]
});
