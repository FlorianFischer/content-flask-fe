import * as types from '../actions/types';
import { omit } from 'lodash';

const initialState = { byId: {} };


const sectionReducer = (state = initialState, action) => {
  switch(action.type) {
    case types.CREATE_PROJECT_SUCCESS:
    case types.SAVE_PROJECT_SUCCESS:
    case types.FETCH_PROJECT_SUCCESS:
    case types.FETCH_PROJECTS_SUCCESS:
      return {
        ...state,
        byId: {
          ...state.byId,
          ...action.payload.sections
        }
      };

    case types.ADD_SECTION:
    case types.ADD_PAGE:
      const newSection = action.payload.section;

      return {
        ...state,
        byId: {
          ...state.byId,
          [newSection._tempId]: newSection
        }
      };

    case types.UPDATE_SECTION:
      const { id, values } = action.payload;
      return {
        ...state,
        byId: {
          ...state.byId,
          [id]: {
            ...state.byId[id],
            ...values
          }
        }
      };

    case types.REMOVE_SECTION:
      return {
        ...state,
        byId: omit(state.byId, [action.payload.sectionId])
      };

    default:
      return state;
  }
};


export default sectionReducer;
