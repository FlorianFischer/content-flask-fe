import AuthService from '../services/AuthService';


const initialState = {
  session: {
    isAuthenticated: !!AuthService.isUserAuthenticated(),
    user: {}
  }
};


export default initialState;
