import * as types from '../actions/types';
import AuthService from '../services/AuthService';


const initialState = {
  isAuthenticated: AuthService.isUserAuthenticated()
};


const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.AUTH_SUCCESS:
      const { user } = action;
      const company = { ...user.company };
      delete user.company;
      return Object.assign({}, state, {
        isAuthenticated: AuthService.isUserAuthenticated(),
        user,
        company
      });
    case types.LOGOUT:
    case types.AUTH_FAILURE:
      return Object.assign({}, state, {
        isAuthenticated: AuthService.isUserAuthenticated(),
        user: {},
        company: {}
      });
    default:
      return state;
  }
};


export default authReducer;
