import * as types from '../actions/types';


const initialState = {
  alphabeticalFilter: 'ASC',
  byId: {}
};


const clientReducer = (state = initialState, action) => {
  switch(action.type) {
    case types.CREATE_CLIENT_SUCCESS:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.client._id]: action.client
        }
      };

    case types.FETCH_CLIENTS_SUCCESS:
      const newById = {};

      action.clients.forEach(client => {
        newById[client._id] = client;
      });

      return {
        ...state,
        byId: { ...state.byId, ...newById }
      };

    default:
      return state;
  }
};


export default clientReducer;
