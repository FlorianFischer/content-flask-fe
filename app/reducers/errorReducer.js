const errorReducer = (state = null, action) => {
  if (action.type === 'RESET_ERROR') {
    return null;
  }

  const { error } = action;

  if (!error || typeof error !== 'string' || !error.length) {
    return state;
  }

  return action.error;
};


export default errorReducer;
