import * as types from '../actions/types';
import _ from 'lodash';

const initialState = {
  stateFilter: 'ACTIVE',
  projectsPerPage: 10,
  page: 1,
  currentProject: null,
  byId: {}
};


const projectReducer = (state = initialState, action) => {
  switch(action.type) {
    case types.CREATE_PROJECT_SUCCESS:
    case types.SAVE_PROJECT_SUCCESS:
    case types.FETCH_PROJECT_SUCCESS:
    case types.FETCH_PROJECTS_SUCCESS:
      return {
        ...state,
        byId: { ...state.byId, ...action.payload.projects }
      };

    case types.DELETE_PROJECT_SUCCESS:
      return {
        ...state,
        byId: _.omit(state.byId, [action.payload.id])
      };

    case types.CHANGE_STATE_VISIBILITY: {
      return {
        ...state,
        stateFilter: action.filter,
      };
    }
    case types.CHANGE_PAGE: {
      return {
        ...state,
        page: action.page
      };
    }
    case types.ADD_CLIENT_PROJECT: {
      return {
        ...state,
        byId: {
          [action.id]: {
            ...state.byId[action.id],
            client: action.client
          }
        }
      };
    }
    case types.ADD_PAGE: {
      const { projectId } = action.payload;
      return {
        ...state,
        byId: {
          ...state.byId,
          [projectId]: {
            ...state.byId[projectId],
            pages: [ ...state.byId[projectId].pages, action.payload.page._tempId ]
          }
        }
      };
    }
    case types.UPDATE_PROJECT: {
      const { id, values } = action.payload;
      return {
        ...state,
        byId: {
          ...state.byId,
          [id]: {
            ...state.byId[id],
            ...values
          }
        }
      };
    }
    // case types.REMOVE_PAGE: {
    //   const { projectId } = action.payload;
    //   return {
    //     ...state,
    //     byId: {
    //       ...state.byId,
    //       [projectId]: {
    //         ...state.byId[projectId],
    //         pages: [ ...state.byId[projectId].pages, action.payload.page._tempId ]
    //       }
    //     }
    //   };
    // }
    case types.SET_CURRENT_PROJECT:
      return {
        ...state,
        currentProject: action.id
      };
    default:
      return state;
  }
};


export default projectReducer;
