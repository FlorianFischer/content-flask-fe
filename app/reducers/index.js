import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import * as types from '../actions/types';
import session from './authReducer';
import projects from './projectReducer';
import error from './errorReducer';
import { reducer as form } from 'redux-form';
import clients from './clientReducer';
import pages from './pageReducer';
import sections from './sectionReducer';


const appReducer = combineReducers({
  form,
  session,
  projects,
  pages,
  clients,
  error,
  sections,
  routing
});


const rootReducer = (state, action) => {
  if (action.type === (types.LOGOUT || types.AUTH_FAILURE)) {
    state = { routing: state.routing };
  }

  return appReducer(state, action);
};


export default rootReducer;
