import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authActions from '../actions/authActions';
import ForgotForm from '../components/ForgotForm';
import { SubmissionError } from 'redux-form';
import errorMessages from '../constants/messagesConstants';


class ForgotContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      success: false
    };

    this.requestReset = this.requestReset.bind(this);
  }


  requestReset(values) {
    return this.props.actions.requestReset(values.email)
      .then(() => this.setState({ success: true }))
      .catch(error => {
        throw new SubmissionError({
          _error: errorMessages.RESET_EMAIL_FAILED
        });
      });
  }


  render() {
    return (
      <ForgotForm
        onSubmit={ this.requestReset }
        success={ this.state.success }
      />
    );
  }
}


ForgotContainer.propTypes = {
  actions: PropTypes.object,
  isAuthenticated: PropTypes.bool.isRequired
};


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch)
  };
}


function mapStateToProps(state, props) {
  return {
    isAuthenticated: state.session.isAuthenticated
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(ForgotContainer);
