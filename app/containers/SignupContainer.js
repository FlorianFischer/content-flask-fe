import React from 'react';
import { PropTypes } from 'prop-types';
import SignupForm from '../components/SignupForm';
import * as authActions from '../actions/authActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import _ from 'lodash';
import { SubmissionError } from 'redux-form';
import * as utils from '../utils/utils';


class SignupContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      success: false
    };

    this.processForm = this.processForm.bind(this);
  }


  processForm(values) {
    return this.props.actions.signup(values)
      .then(() => this.setState({ success: true }))
      .catch(error => {
        switch(error.message) {
          case 'form-error':
            _.forOwn(error.errors, value => {
              throw new SubmissionError({
                _error: utils.toErrorMessage(value)
              });
            });
            break;
          default:
            throw new SubmissionError({
              _error: utils.toErrorMessage(error.message)
            });
        }
      });
  }


  render() {
    return (
      <div className="auth-container">
        <div className="auth-side"></div>
        <div className="auth-form">
          <SignupForm onSubmit={ this.processForm }/>
        </div>
        {(this.props.isAuthenticated || this.state.success) && (
          <Redirect to={'/projects'}/>
        )}
      </div>
    );
  }
}


SignupContainer.propTypes = {
  actions: PropTypes.object,
  isAuthenticated: PropTypes.bool.isRequired
};


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch)
  };
}


function mapStateToProps(state, props) {
  return {
    isAuthenticated: state.session.isAuthenticated
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(SignupContainer);
