import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authActions from '../actions/authActions';
import ResetForm from '../components/ResetForm';
import { SubmissionError } from 'redux-form';
import errorMessages from '../constants/messagesConstants';
import * as utils from '../utils/utils';


class ResetContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tokenInValidMessage: null,
      success: false
    };

    this.resetPassword = this.resetPassword.bind(this);
  }


  resetPassword(values) {
    return this.props.actions.resetPassword(values.password, this.props.match.params.token)
      .then(() => this.setState({ success: true }))
      .catch(error => {
        throw new SubmissionError({
          _error: utils.toErrorMessage(error.message)
        });
      });
  }


  render() {
    return (
      <ResetForm
        onSubmit={ this.resetPassword }
        success={ this.state.success }
        tokenInValidMessage={ this.state.tokenInValidMessage }
      />
    );
  }
}


ResetContainer.propTypes = {
  actions: PropTypes.object,
  match: PropTypes.object,
  isAuthenticated: PropTypes.bool.isRequired
};


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch)
  };
}


function mapStateToProps(state, props) {
  return {
    isAuthenticated: state.session.isAuthenticated
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(ResetContainer);
