import React from 'react';
import { PropTypes } from 'prop-types';
import q from 'q';
import { Loader, Breadcrumb, Icon } from 'semantic-ui-react';
import Spinner from '../components/Spinner';
import { connect } from 'react-redux';
import * as types from '../actions/types';


class MainContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = { isLoading: false };
  }


  // LIFECYCLE


  componentDidMount() {
    const { promises } = this.props;

    if (promises && promises.length) {
      this.setState({ isLoading: true }, () => {
        q.all(promises).finally(() => this.setState({ isLoading: false }));
      });
    }
  }


  // RENDER


  render() {
    const toRender = this.state.isLoading
      ? <Spinner />
      : this.props.children;

    const { breadcrumb, error } = this.props;

    return (
      <div className={ `main-container ${this.props.pageName}` }>
        <div className={ `error global ${error ? 'visible' : ''}` }>
          <span>{ error }</span>
          <Icon
           name="delete"
           onClick={ this.props.resetError }
           link/>
        </div>
        <div className="top-nav">
          <Breadcrumb>
            <Breadcrumb.Section active>{ breadcrumb }</Breadcrumb.Section>
          </Breadcrumb>
        </div>
        <div className="content">
          { toRender }
        </div>
      </div>
    );
  }
}


MainContainer.propTypes = {
  promises: PropTypes.array,
  breadcrumb: PropTypes.string.isRequired,
  pageName: PropTypes.string,
  children: PropTypes.node,
  error: PropTypes.string,
  resetError: PropTypes.func
};


function mapDispatchToProps(dispatch) {
  return {
    resetError: () => dispatch({ type: types.RESET_ERROR })
  };
}


function mapStateToProps(state, props) {
  return {
    error: state.error
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);
