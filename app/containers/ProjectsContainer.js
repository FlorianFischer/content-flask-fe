import React from 'react';
import { PropTypes } from 'prop-types';
import MainContainer from'./MainContainer';
import ProjectCard from '../components/ProjectCard';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';
import Switch from '../components/Switch';
import Pagination from '../components/Pagination';
import ActionButton from '../components/ActionButton';
import * as projectActions from '../actions/projectActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getVisibleProjects, getVisibleProjectsPaged } from '../selectors/projectSelectors';


class ProjectsContainer extends React.Component {
  constructor(props) {
    super(props);
    this.promises = [];
  }


  // LIFECYCLE


  componentWillMount() {
    this.promises.push(this.props.actions.getProjects());
  }


  componentDidMount() {
    this.props.filterProjects(this.props.stateFilter || 'ACTIVE');
    this.props.changePage(this.props.page);
  }


  // EVENTS


  handleCreateProject() {
    return this.props.actions.createProject()
      .then(project => {
        this.props.history.push(`/projects/${project._id}`);
      });
  }


  handleProjectClick(project) {
    this.props.history.push(`/projects/${project._id}`);
  }


  handleChangeStatus(status = 'ACTIVE') {
    this.props.filterProjects(status);
    this.props.changePage(1);
  }


  handleChangePage(e, { page }) {
    this.props.changePage(page);
  }


  // RENDER


  render() {
    const { projects, stateFilter, projectsPerPage, projectCount, page } = this.props;
    const pageCount = Math.ceil(projectCount / projectsPerPage);

    return (
      <MainContainer pageName="projects" promises={ this.promises } breadcrumb="projects">
        <div className="controls">
          <Switch
           isLeft={ stateFilter === 'ACTIVE' }
           onLeftClick={ this.handleChangeStatus.bind(this, 'ACTIVE') }
           onRightClick={ this.handleChangeStatus.bind(this, 'DRAFT') }
           leftContent="Active"
           rightContent="Drafts"/>

          {projectCount > projectsPerPage &&
            <Pagination
             onPageChange={ this.handleChangePage.bind(this) }
             currentPage={ page }
             pageCount={ pageCount }/>
         }
        </div>

        <div className="project-grid">
          {projects.map((project, i) => {
            const firstInRow = i === 0 || i % (projectsPerPage / 2) === 0;
            return (
              <div className={ `grid-item ${firstInRow ? 'first' : ''}` } key={ i }>
                <ProjectCard
                 name={ project.name }
                 progress={ project.progress }
                 state={ project.state }
                 onClick={ this.handleProjectClick.bind(this, project) }
                />
              </div>
            );
          })}
        </div>

        <ActionButton items={ [{
          icon: 'plus',
          action: this.handleCreateProject.bind(this),
          label: 'New project',
          isAsync: true
        }] }/>
      </MainContainer>
    );
  }
}


ProjectsContainer.propTypes = {
  history: PropTypes.object,
  location: PropTypes.object,
  actions: PropTypes.object,
  projects: PropTypes.array.isRequired,
  'history.push': PropTypes.func,
  filterProjects: PropTypes.func,
  changePage: PropTypes.func,
  stateFilter: PropTypes.string,
  page: PropTypes.number,
  projectsPerPage: PropTypes.number,
  projectCount: PropTypes.number
};


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(projectActions, dispatch),
    filterProjects: filter => dispatch(projectActions.changeStateVisibility(filter)),
    changePage: page => dispatch(projectActions.changePage(page))
  };
}


function mapStateToProps(state, props) {
  state = state.projects;

  return {
    projects: getVisibleProjectsPaged(state),
    projectCount: getVisibleProjects(state).length,
    stateFilter: state.stateFilter,
    page: state.page,
    projectsPerPage: state.projectsPerPage
  };
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ProjectsContainer));
