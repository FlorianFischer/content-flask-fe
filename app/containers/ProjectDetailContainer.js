import React from 'react';
import { PropTypes } from 'prop-types';
import MainContainer from'./MainContainer';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import * as projectActions from '../actions/projectActions';
import GeneralInfoForm from '../components/GeneralInfoForm';
import _ from 'lodash';
import { getSelectSortedClients } from '../selectors/clientSelectors';
import * as clientActions from '../actions/clientActions';
import FormBuilder from '../components/FormBuilder';
import ActionButton from '../components/ActionButton';
import IconNamed from '../components/IconNamed';
import { Button, Icon } from 'semantic-ui-react';
import { getCurrentPages, getCurrentProject } from '../selectors/pageSelectors';
import AsyncConfirm from '../components/AsyncConfirm';


class ProjectDetailContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      showConfirmDelete: false,
      showGeneralInfo: true
    };
    this.promises = [];
    props.setCurrentProject(this.props.match.params.id);
  }


  // LIFECYCLE


  componentWillMount() {
    const { id } = this.props.match.params;
    this.promises.push(
      this.props.projectActions.getSingleProject(id),
      this.props.clientActions.getClients()
    );
  }


  componentWillReceiveProps(nextProps) {
    for (const index in nextProps) {
      if (nextProps[index] !== this.props[index]) {
        // console.log(index, this.props[index], '-->', nextProps[index]);
      }
    }
  }


  componentWillUnmount() {
    this.props.setCurrentProject(null);
  }


  // EVENTS


  handleSaveProject(mainButtonClick) {
    if (this.state.isLoading) return;
    if (mainButtonClick) this.setState({ isLoading: true });

    const { saveProject } = this.props.projectActions;
    return saveProject(this.props.match.params.id)
      .finally(() => this.setState({ isLoading: false }));
  }


  handleDeleteProject() {
    const { deleteProject } = this.props.projectActions;
    return deleteProject(this.props.match.params.id)
      .then(() => this.props.history.push('/projects'));
  }


  // RENDER


  render() {
    const { id } = this.props.match.params;
    const { isLoading, showGeneralInfo } = this.state;
    const { project } = this.props;
    const initialValues = {
      name: project.name,
      deadline: project.deadline,
      client: project.client,
      password_protection: project.password_protection
    };

    const actions = [
      {
        icon: 'plus',
        action: () => console.log('lala'),
        label: 'Add element',
      }, {
        icon: 'save',
        action: this.handleSaveProject.bind(this),
        label: 'Save project',
        isAsync: true
      }
    ];

    return (
      <MainContainer pageName="project-detail" promises={ this.promises } breadcrumb={ project.name || 'Unnamed project' }>
        <div className="controls">
          <IconNamed
           title="General Info"
           subtitle="We need this information to publish your project"
           icon="star"
           color="blue" />

          <div>
            <Icon
             name={ `chevron ${showGeneralInfo ? 'up' : 'down'}` }
             size="small"
             onClick={ () => this.setState({ showGeneralInfo: !showGeneralInfo }) } />
            <Button
             className="tertiary"
             icon="trash"
             content="Delete project"
             size="small"
             loading={ this.state.isDeleting }
             onClick={ () => this.setState({ showConfirmDelete: true }) } />
            <AsyncConfirm
             confirmText="Delete project"
             title="Are you sure?"
             subtitle="The project will be deleted permanently"
             open={ this.state.showConfirmDelete }
             onCancel={ () => this.setState({ showConfirmDelete: false }) }
             onConfirm={ this.handleDeleteProject.bind(this) }/>
            <Button
             className="primary"
             icon="save"
             content="Save"
             loading={ this.state.isLoading }
             onClick={ this.handleSaveProject.bind(this, true) } />
          </div>
        </div>

        {showGeneralInfo &&
          <GeneralInfoForm
           projectId={ id }
           initialValues={ initialValues }
           clients={ this.props.clients }
           isLoading={ isLoading }/>
        }

        <FormBuilder
         pages={ this.props.pages }
         projectId={ id } />

        <ActionButton items={ actions } />
      </MainContainer>
    );
  }
}


ProjectDetailContainer.propTypes = {
  history: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.shape({
    params: PropTypes.shape({ id: PropTypes.string })
  }),
  projectActions: PropTypes.object,
  clientActions: PropTypes.object,
  project: PropTypes.object.isRequired,
  pages: PropTypes.array.isRequired,
  clients: PropTypes.array,
  setCurrentProject: PropTypes.func
};


function mapDispatchToProps(dispatch) {
  return {
    projectActions: bindActionCreators(projectActions, dispatch),
    clientActions: bindActionCreators(clientActions, dispatch),
    setCurrentProject: id => dispatch(projectActions.setCurrentProject(id))
  };
}


function mapStateToProps(state, props) {
  return {
    project: getCurrentProject(state) || {},
    clients: getSelectSortedClients(state.clients),
    pages: getCurrentPages(state)
  };
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ProjectDetailContainer));
