import React from 'react';
import { PropTypes } from 'prop-types';
import LoginForm from '../components/LoginForm';
import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form';
import { bindActionCreators } from 'redux';
import * as utils from '../utils/utils';
import * as authActions from '../actions/authActions';
import { Redirect } from 'react-router-dom';
import sideBackGround from '../assets/note_book.jpg';


class LoginContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      success: false
    };

    this.processForm = this.processForm.bind(this);
  }


  processForm(values) {
    const { email, password } = values;

    return this.props.actions.login(email, password)
      .then(() => {
        if (this.props.isAuthenticated) return;
        this.setState({ success: true });
      })
      .catch(error => {
        if (error.message === 'incorrect-credentials') {
          throw new SubmissionError({
            _error: utils.toErrorMessage(error.message)
          });
        }
      });
  }


  render() {
    const backgroundStyle = { backgroundImage: sideBackGround };

    return (
      <div className="auth-container">
        <div className="auth-side"></div>
        <div className="auth-form">
          <LoginForm onSubmit={ this.processForm } />
        </div>
        {(this.props.isAuthenticated || this.state.success) && (
          <Redirect to={ '/projects' }/>
        )}
      </div>
    );
  }
}


LoginContainer.propTypes = {
  actions: PropTypes.object,
  isAuthenticated: PropTypes.bool.isRequired
};


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch)
  };
}


function mapStateToProps(state, props) {
  return {
    isAuthenticated: state.session.isAuthenticated
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
