import errorMessages from '../constants/messagesConstants';

export function toErrorKey(error) {
  error = error.replace(/-/g, '_').toUpperCase();
  return error;
}


export function toErrorMessage(error) {
  const message = errorMessages[toErrorKey(error)] || errorMessages.GENERAL_ERROR;
  return message;
}


export function insertAt(array, item, index) {
  array = [ ...array ];
  array.splice(index, 0, item);
  return array;
}


export function shiftItem(array, item, toIndex) {
  array = [ ...array ];
  const fromIndex = array.indexOf(item);
  array.splice(fromIndex, 1);
  array.splice(toIndex, 0, item);
  return array;
}


export function removeItem(array, item) {
  array = [ ...array ];
  const index = array.indexOf(item);
  array.splice(index, 1);
  return array;
}
