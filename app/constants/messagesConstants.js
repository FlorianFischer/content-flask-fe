export default {
  FIELD_MISSING: 'Please fill out the entire form',
  WRONG_PASSWORD: 'Wrong password',
  USER_NOT_FOUND: 'This username does not exist',
  USERNAME_TAKEN: 'Sorry, but this username is already taken',
  GENERAL_ERROR: 'Something went wrong, please try again',

  INCORRECT_CREDENTIALS: 'Your email or password does not match',
  COMPANY_TAKEN: 'This company already exists',
  EMAIL_TAKEN: 'This email address is already in use',

  RESET_EMAIL_FAILED: 'We couldn\'t send your email. Please try again',
  RESET_EMAIL_SUCCESS: 'If this email exists, we have sent you a link to reset your password',
  EMAIL_TOKEN_INVALID: 'Your link is not valid anymore. Please request a new one',
  RESET_SUCCESS: 'Your password was succesfully changed. You can log in now',

  // PROJECTS

  FETCH_PROJECT_FAILURE: 'Something went wrong loading your project. Please reload and try again.',
  SAVE_PROJECT_FAILURE: 'Something went wrong saving your project. Please reload and try again.',
  DELETE_PROJECT_FAILURE: 'We could not delete this project. Please reload and try again.',

  // CLIENTS

  CLIENT_ALREADY_EXISTS: 'A client with this email is already registered.',
  FETCH_CLIENTS_FAILURE: 'There was a problem loading your clients. Please reload and try again',

  // PAGES

  CREATE_PAGE_FAILURE: 'There was a problem creating a new page. Please reload and try again',
  DELETE_PAGE_FAILURE: 'There was a problem removing this page. Please reload and try again',
};
