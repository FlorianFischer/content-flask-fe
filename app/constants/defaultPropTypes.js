import { PropTypes } from 'prop-types';


export default {
  className: PropTypes.string,
  currentValue: PropTypes.any,
  custom: PropTypes.object,
  fieldClass: PropTypes.string,
  input: PropTypes.object.isRequired,
  inputLabel: PropTypes.string,
  label: PropTypes.string,
  meta: PropTypes.shape({
    error: PropTypes.string,
    touched: PropTypes.bool,
  }),
  required: PropTypes.bool,
  topLabel: PropTypes.string,
  type: PropTypes.string,
  icon: PropTypes.string,
  name: PropTypes.string,
  iconPosition: PropTypes.string,
  placeholder: PropTypes.string
};
