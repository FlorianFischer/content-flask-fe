import React from 'react';
import PropTypes from 'prop-types';
import Input from '../components/ControlledInput';
import { Form, Message, Grid, Button, Loader, Icon } from 'semantic-ui-react';


class SectionPreview extends React.Component {
  render() {
    const { name } = this.props;

    return (<div className="c-card section preview">
      <Form>
        <Grid>
          <Grid.Row>
            <Grid.Column width={ 16 }>
              <div className="form-item-heading">
                <div key="drag-handle" className="headline circle-before orange drag-handle">
                  <span>{ name }</span>
                  <span className="subtitle">Section details</span>
                </div>
                <div className="item-controls">
                  <Icon
                   name="chevron up"
                   size="small"/>
                  <Button
                   icon="trash"
                   size="small"
                   content="Delete"
                   className="secondary"/>
                </div>
              </div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={ 16 }>
              <Input
               initial={ name }
               label="Section name"
               placeholder="Section name"/>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Form>
    </div>);
  }
}


SectionPreview.propTypes = {
  name: PropTypes.string
};


export default SectionPreview;
