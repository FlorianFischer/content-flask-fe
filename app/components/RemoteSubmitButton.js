import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { submit } from 'redux-form';
import { Button } from 'semantic-ui-react';


const RemoteSubmitButton = props => {
  const { dispatch, formName, text, loading, disabled } = props;

  return (
    <Button
      className="primary"
      size="large"
      loading={ loading }
      disabled={ disabled }
      onClick={ () => dispatch(submit(formName)) }>
      { text }
    </Button>
  );
};


RemoteSubmitButton.propTypes = {
  dispatch: PropTypes.func.isRequired,
  formName: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  loading: PropTypes.bool,
  disabled: PropTypes.bool
};


export default connect()(RemoteSubmitButton);
