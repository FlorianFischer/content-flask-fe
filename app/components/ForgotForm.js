import React from 'react';
import { PropTypes } from 'prop-types';
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react';
import FormError from './FormError';
import { Field, reduxForm } from 'redux-form';
import TextField from './TextField';
import { Link } from 'react-router-dom';
import messages from '../constants/messagesConstants';
import logo from '../assets/logo_color.svg';


const validate = values => {
  const errors = {};

  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email';
  }

  if (!values.email) {
    errors.email = 'Required';
  }

  return errors;
};


class ForgotForm extends React.Component {
  render() {
    const { handleSubmit, error, success, submitting, invalid } = this.props;

    return (
      <Grid
        textAlign="right"
        style={{ height: '100%' }}
        verticalAlign="middle"
        className="auth-container"
      >
        <Grid.Column>
          <Form className="auth-form single-input" size="large" onSubmit={ handleSubmit }>
            <Image className="logo" src={ logo } />
            <Segment>
              {error &&
                <Message key={ error } negative>
                  <Message.Header>{ error }</Message.Header>
                </Message>
              }
              {success &&
                <Message key="forgot-success" positive>
                  <Message.Header>{ messages.RESET_EMAIL_SUCCESS }</Message.Header>
                </Message>
              }
              {(!error && !success) && (
                <div>
                  <Field
                    placeholder="you@company.com"
                    type="email"
                    name="email"
                    component={ TextField }
                  />
                  <div className="actions primary">
                    <Link className="passive" to="/login">Login</Link>
                    <Button
                      className="primary"
                      loading={ submitting }
                      disabled={ invalid }
                      size="large">
                      Request reset
                    </Button>
                  </div>
                </div>
              )}
            </Segment>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}


ForgotForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.string,
  submitting: PropTypes.bool,
  success: PropTypes.bool,
  invalid: PropTypes.bool
};


export default reduxForm({
  form: 'forgotForm',
  validate
})(ForgotForm);
