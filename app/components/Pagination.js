import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Icon } from 'semantic-ui-react';


const Pagination = props => {
  const { currentPage, onPageChange } = props;
  const pageCount = props.pageCount || 1;


  return (
    <Menu pagination>
      <Menu.Item
       page={ currentPage - 1 }
       onClick={ onPageChange }
       disabled={ currentPage === 1 }
       as="a"
       icon>
        <Icon name="left chevron" />
      </Menu.Item>

      {[...Array(pageCount).keys()].map(index => {
        index = index + 1;
        return (<Menu.Item
                active={ currentPage === index }
                page={ index }
                key={ index }
                onClick={ onPageChange }
                as="a">
                { index }
               </Menu.Item>);
      })}

      <Menu.Item
       as="a"
       page={ currentPage + 1 }
       onClick={ onPageChange }
       disabled={ currentPage === pageCount }
       icon>
        <Icon name="right chevron" />
      </Menu.Item>
    </Menu>
  );
};


Pagination.propTypes = {
  onPageChange: PropTypes.func,
  currentPage: PropTypes.number.isRequired,
  pageCount: PropTypes.number.isRequired
};


export default Pagination;
