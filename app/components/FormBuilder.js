import React from 'react';
import PropTypes from 'prop-types';
import { Tab, Menu } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as pageActions from '../actions/pageActions';
import * as projectActions from '../actions/projectActions';
import { Icon } from 'semantic-ui-react';
import Page from './Page';
import q from 'q';
import FloatingSideBar from '../components/FloatingSideBar';
import ReactCursorPosition from 'react-cursor-position';


class FormBuilder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0
    };
  }


  // INTERNAL


  createPanesfromPages(pages) {
    const panes = pages.map(page => {
      if (!page) return null;
      const id = page._id || page._tempId;

      return {
        menuItem: <Menu.Item key={ id }>{ page.name }</Menu.Item>,
        pane: (
          <Tab.Pane key={ id }>
            <Page
             form={ id }
             page={ page }
             projectId={ this.props.projectId }
             handleDeletePage={ this.handleDeletePage.bind(this) }
             initialValues={{
               name: page.name,
               description: page.description,
               has_description: page.has_description
             }}/>
          </Tab.Pane>
        )
      };
    }).filter(pane => !!pane);

    panes.push({
      menuItem:
        <Menu.Item
         className="add-item"
         onClick={ this.handleAddPage.bind(this) }
         key="add-item">
          <Icon name="plus square outline" size="large"/>Add page
        </Menu.Item>
    });

    return panes;
  }


  // EVENTS


  handleTabChange(event, { activeIndex }) {
    if (activeIndex !== this.props.pages.length) {
      this.setState({ activeIndex });
      return;
    }
  }


  handleAddPage() {
    this.props.addPage({ name: 'New page' }, this.props.projectId);
    this.setState({ activeIndex: this.props.pages.length });
  }


  handleDeletePage(page) {
    let { activeIndex } = this.state;
    activeIndex = !!activeIndex ? 0 : activeIndex - 1;

    if (!page._id) {
      this.props.deleteUnsavedPage(page._tempId);
      this.setState({ activeIndex });
      return q.resolve();
    }

    return this.props.pageActions.deletePage(this.props.projectId, page._id)
      .then(() => this.setState({ activeIndex }));
  }


  // RENDER


  render() {
    const { pages } = this.props;
    const panes = this.createPanesfromPages(pages);

    return (
      <div className="form-builder">
        <Tab
         renderActiveOnly={ false }
         activeIndex={ this.state.activeIndex }
         onTabChange={ this.handleTabChange.bind(this) }
         panes={ panes } />

        <FloatingSideBar />
      </div>
    );
  }
}


FormBuilder.propTypes = {
  pages: PropTypes.array.isRequired,
  pageActions: PropTypes.object.isRequired,
  projectId: PropTypes.string.isRequired,
  addPage: PropTypes.func,
  deleteUnsavedPage: PropTypes.func
};


function mapDispatchToProps(dispatch) {
  return {
    pageActions: bindActionCreators(pageActions, dispatch),
    addPage: (page, projectId) => dispatch(pageActions.addPage(page, projectId)),
    deleteUnsavedPage: id => dispatch(pageActions.removePage(id))
  };
}


export default connect(
  null,
  mapDispatchToProps
)(FormBuilder);
