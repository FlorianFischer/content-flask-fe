import React from 'react';
import { Message } from 'semantic-ui-react';
import { PropTypes } from 'prop-types';


class FormError extends React.Component {
  render() {
    const keys = Object.keys(this.props.errors);
    const errors = keys.filter(error => this.props.errors[error]);

    return (
      <div>
        {errors.map(error =>
          <Message key={ error } negative onDismiss={ this.props.onDismiss.bind(null, error) }>
            <Message.Header>{ this.props.errors[error] }</Message.Header>
          </Message>)
        }
      </div>
    );
  }
}


FormError.propTypes = {
  errors: PropTypes.object,
  onDismiss: PropTypes.func
};


export default FormError;
