import React from 'react';
import Routes from '../routes';
import * as authActions from '../actions/authActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { Button } from 'semantic-ui-react';
import Spinner from './Spinner';
import SideBar from './SideBar';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd';


class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true
    };
  }


  componentDidMount() {
    this.setState({ isLoading: true });

    return this.props.actions.checkAuth()
      .finally(() => this.setState({ isLoading: false }));
  }


  render() {
    if (this.state.isLoading) return <div><Spinner /></div>;

    return (
      <div className="app-container">
        {this.props.isAuthenticated && ( <SideBar /> )}
        { Routes }
      </div>
    );
  }
}


App.propTypes = {
  actions: PropTypes.object,
  children: PropTypes.object,
  isAuthenticated: PropTypes.bool.isRequired
};


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch)
  };
}


function mapStateToProps(state, props) {
  return {
    isAuthenticated: state.session.isAuthenticated
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(DragDropContext(HTML5Backend)(App));
