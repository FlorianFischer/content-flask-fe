import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';


const AddNewField = props => {
  const { label, content, onClick, spaceLabel } = props;
  return (
    <div className={ `field ${spaceLabel ? 'space-label' : ''}` }>
      {label && <label>{ label }</label>}
      <div className="placeholder"
           onClick={ onClick }>
        <Icon name="plus" size="small" />
        <a>{ content }</a>
      </div>
    </div>
  );
};


AddNewField.propTypes = {
  label: PropTypes.string,
  content: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  spaceLabel: PropTypes.bool
};


export default AddNewField;
