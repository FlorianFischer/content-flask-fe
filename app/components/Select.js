import React from 'react';
import PropTypes from 'prop-types';
import defaultPropTypes from '../constants/defaultPropTypes';
import Select from 'react-select';


class SelectWrapper extends React.Component {
  render() {
    const {
      input,
      label,
      type,
      icon,
      iconPosition,
      name,
      placeholder,
      options
    } = this.props;
    const { touched, error, warning, initial } = this.props.meta;

    return (
      <div className="field">
        {label && <label>{ label }</label>}
        <Select
         name={ name }
         value={ input.value }
         onChange={ input.onChange }
         placeholder={ placeholder }
         options={ options }/>
      </div>
    );
  }
}


SelectWrapper.propTypes = defaultPropTypes;


export default SelectWrapper;
