import React from 'react';
import PropTypes from 'prop-types';
import { DragSource } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';
import * as dragTypes from '../constants/dragTypes';


class SectionDragItem extends React.Component {
  componentDidMount() {
    this.props.connectDragPreview(getEmptyImage());
  }


  render() {
    const { isDragging, connectDragSource } = this.props;

    return connectDragSource(
      <div className="item drag-handle"></div>
    );
  }
}


SectionDragItem.propTypes = {
  isDragging: PropTypes.bool.isRequired,
  connectDragSource: PropTypes.func.isRequired,
  connectDragPreview: PropTypes.func.isRequired
};


const sectionSource = {
  beginDrag(props) {
    return {};
  }
};


function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
  };
}


export default DragSource(dragTypes.SECTION, sectionSource, collect)(SectionDragItem);
