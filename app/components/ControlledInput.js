import React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'semantic-ui-react';


class ControlledInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: props.initial };
  }


  // LIFECYCLE


  componentWillReceiveProps(nextProps) {
    this.setState({ value: nextProps.initial });
  }


  // EVENTS


  handleChange(event) {
    const { value } = event.target;
    this.setState({ value });

    if (this.props.onChange) this.props.onChange(event, value);
  }


  // RENDER


  render() {
    const {
      label,
      placeholder,
      type,
      onBlur
    } = this.props;

    return(
      <div className={ `field ${label ? 'has-label' : ''}` }>
        {label && <label>{ label }</label>}
        <Input
         placeholder={ placeholder }
         type={ type || 'text' }
         className="wrapped"
         value={ this.state.value }
         onChange={ this.handleChange.bind(this) }
         onBlur={ (event) => onBlur ? onBlur(event, event.target.value) : null } />
      </div>
    );
  }
}


ControlledInput.propTypes = {
  initial: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func
};


export default ControlledInput;
