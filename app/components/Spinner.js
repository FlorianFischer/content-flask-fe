import React from 'react';


const Spinner = () =>
  <div className="folding-cube">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>;


export default Spinner;
