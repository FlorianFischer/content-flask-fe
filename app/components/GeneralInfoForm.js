import React from 'react';
import PropTypes from 'prop-types';
import { Message, Grid, Button, Loader, Icon } from 'semantic-ui-react';
import Select from './Select';
import { Form, Field, reduxForm, SubmissionError, change } from 'redux-form';
import TextField from './TextField';
import SingleDatePicker from './SingleDatePicker';
import AddNewField from './AddNewField';
import NewClientModal from './modals/NewClientModal';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as clientActions from '../actions/clientActions';
import * as projectActions from '../actions/projectActions';
import IconBoxed from '../components/IconBoxed';
import _ from 'lodash';
import Checkbox from '../components/Checkbox';


class GeneralInfoForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOpen: false
    };
  }


  // EVENTS


  handleCreateClient(data) {
    return this.props.clientActions.createClient(data)
      .then(client => {
        this.props.addClientToProject(client._id, this.props.projectId);
        this.props.changeFieldValue('client', client._id);
        this.handleFormChange('client', null, client._id);
        this.setState({ modalOpen: false });
      })
      .catch(error => {
        throw new SubmissionError({
          _error: error
        });
      });
  }


  handleChangeClient(event, value) {
    if (value === null) {
      this.props.addClientToProject('', this.props.projectId);
    }

    this.handleFormChange('client', null, value);
  }


  handleFormChange(field, event, value) {
    this.props.updateProject(this.props.projectId, { [field]: value });
  }


  // RENDER


  render() {
    const { props } = this;
    const { initialValues, isLoading } = props;

    return (
      <div className="ui form c-card">
        <Form>
          <Grid>
            <Grid.Row>
              <Grid.Column width={ 8 }>
                <Field
                  placeholder="e.g. Amazon Redesign"
                  type="text"
                  name="name"
                  label="project name"
                  onBlur={ this.handleFormChange.bind(this, 'name') }
                  component={ TextField }/>
              </Grid.Column>
              {!!props.clients.length && (
                <Grid.Column width={ 5 }>
                  <Field
                   name="client"
                   label="client"
                   options={ props.clients }
                   value={ initialValues.client }
                   onChange={ this.handleChangeClient.bind(this) }
                   normalize={ selection => selection && selection.value }
                   placeholder="Select a client"
                   component={ Select }/>
                </Grid.Column>
              )}
              <Grid.Column width={ 3 }>
                <AddNewField
                 content="Add new client"
                 label={ props.clients.length ? null : 'client' }
                 onClick={ () => this.setState({ modalOpen: true }) }
                 spaceLabel={ !!props.clients.length }/>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width={ 3 }>
                <Field
                  name="deadline"
                  label="Deadline"
                  onChange={ this.handleFormChange.bind(this, 'deadline') }
                  component={ SingleDatePicker }/>
              </Grid.Column>
              <Grid.Column width={ 5 }>
                <Field
                  name="password_protection"
                  label="Password Protection"
                  onChange={ this.handleFormChange.bind(this, 'password_protection') }
                  component={ Checkbox } />
              </Grid.Column>
            </Grid.Row>
          </Grid>

          {this.state.modalOpen && (
            <NewClientModal
             modalOpen={ this.state.modalOpen }
             onClose={ () => this.setState({ modalOpen: false }) }
             onSubmit={ this.handleCreateClient.bind(this) }/>
          )}
        </Form>
      </div>
    );
  }
}


GeneralInfoForm.propTypes = {
  clientActions: PropTypes.object.isRequired,
  addClientToProject: PropTypes.func.isRequired,
  projectId: PropTypes.string.isRequired,
  clients: PropTypes.array,
  changeFieldValue: PropTypes.func,
  isLoading: PropTypes.bool,
  updateProject: PropTypes.func
};


function mapDispatchToProps(dispatch) {
  return {
    clientActions: bindActionCreators(clientActions, dispatch),
    addClientToProject: (client, id) => dispatch(projectActions.addClientToProject(client, id)),
    changeFieldValue: (field, value) => dispatch(change('generalInfoForm', field, value)),
    updateProject: (id, values) => dispatch(projectActions.updateProject(id, values))
  };
}


export default connect(
  null,
  mapDispatchToProps
)(reduxForm({
  form: 'generalInfoForm'
})(GeneralInfoForm));
