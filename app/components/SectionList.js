import React from 'react';
import PropTypes from 'prop-types';
import Section from '../components/section';
import { connect } from 'react-redux';
import * as sectionActions  from '../actions/sectionActions';
import CustomDragLayer from '../components/CustomDragLayer';
import AsyncConfirm from '../components/AsyncConfirm';
import q from 'q';


class SectionList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showLastSectionWarning: false
    };
  }


  // LIFECYCLE


  handleReorder(index, id) {
    const { reorderSection } = sectionActions;
    this.props.dispatch(reorderSection(this.props.pageId, id, index));
  }


  handleAdd(index) {
    const { addSection } = sectionActions;
    this.props.dispatch(addSection(this.props.pageId, index));
  }


  handleDeleteSection(id) {
    if (this.props.sections.length === 1) {
      return this.setState({ showLastSectionWarning: true });
    }

    const action = sectionActions.removeSection(this.props.pageId, id);
    this.props.dispatch(action);
  }


  // RENDER


  render() {
    const { sections, pageId } = this.props;

    return (
      <div className="section-list">
        {sections.map((section, index) => {
          if (!section) return null;

          return (
            <Section
             key={ `section${section._id || section._tempId}` }
             index={ index }
             onReorder={ this.handleReorder.bind(this) }
             onRemove={ this.handleDeleteSection.bind(this) }
             onAdd={ this.handleAdd.bind(this) }
             section={ section } />
          );
        })}
        <AsyncConfirm
         confirmText="Ok"
         title="Cannot delete section"
         subtitle="Each page needs at least one section"
         noCancel={ true }
         onCancel={ () => this.setState({ showLastSectionWarning: false }) }
         open={ this.state.showLastSectionWarning }/>
        <CustomDragLayer />
      </div>
    );
  }
}


SectionList.propTypes = {
  sections: PropTypes.array,
  pageId: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired
};


export default connect()(SectionList);
