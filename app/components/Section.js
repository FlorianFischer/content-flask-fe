import React from 'react';
import PropTypes from 'prop-types';
import { Form, Message, Grid, Button, Loader, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import TextField from './TextField';
import { Field, reduxForm } from 'redux-form';
import * as sectionActions from '../actions/sectionActions';
import { DragSource, DropTarget } from 'react-dnd';
import Input from '../components/ControlledInput';
import { findDOMNode } from 'react-dom';
import * as dragTypes from '../constants/dragTypes';


class Section extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isExpanded: true
    };
  }


  // EVENTS


  handleUpdateSection(field, event, value) {
    const id = this.props.section._id || this.props.section._tempId;
    const action = sectionActions.updateSection(id, { [field]: value });
    this.props.dispatch(action);
  }


  // RENDER


  render() {
    const { isExpanded } = this.state;
    const {
      connectDragSource,
      connectDragPreview,
      connectDropTarget,
      isDragging,
      isOver,
      section,
      item,
      onRemove
    } = this.props;

    return connectDropTarget(
      connectDragPreview(
        <div>
          {(!isDragging && isOver && !item.id) && (
            <div className="drop-area">
            </div>
          )}
          <div className={ `c-card section ${isDragging ? 'is-dragging' : ''}` }>
            <Form>
              <Grid>
                <Grid.Row>
                  <Grid.Column width={ 16 }>
                    <div className="form-item-heading">
                      {connectDragSource(
                        <div key="drag-handle" className="headline circle-before orange drag-handle">
                          <span>{ this.props.section.name }</span>
                          <span className="subtitle">Section details</span>
                        </div>
                      )}
                      <div className="item-controls">
                        <Icon
                         name={ `chevron ${isExpanded ? 'up' : 'down'}` }
                         size="small"
                         onClick={ () => this.setState({ isExpanded: !isExpanded }) }/>
                        <Button
                         icon="trash"
                         size="small"
                         content="Delete"
                         className="secondary"
                         onClick={ () => onRemove(section._id || section._tempId) }/>
                      </div>
                    </div>
                  </Grid.Column>
                </Grid.Row>
                {isExpanded && <Grid.Row>
                  <Grid.Column width={ 16 }>
                    <Input
                     initial={ section.name }
                     label="Section name"
                     placeholder="Section name"
                     onBlur={ this.handleUpdateSection.bind(this, 'name') } />
                  </Grid.Column>
                </Grid.Row>}
              </Grid>
            </Form>
          </div>
        </div>
      )
    );
  }
}


Section.propTypes = {
  section: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
  connectDragSource: PropTypes.func.isRequired,
  connectDragPreview: PropTypes.func.isRequired,
  connectDropTarget: PropTypes.func.isRequired,
  form: PropTypes.string,
  index: PropTypes.number,
  isDragging: PropTypes.bool,
  isOver: PropTypes.bool,
  item: PropTypes.object,
  onRemove: PropTypes.func.isRequired
};


const sectionSource = {
  beginDrag(props) {
    const { section } = props;
    return {
      id: section._id || section._tempId,
      index: props.index
    };
  }
};


const sectionTarget = {
  hover(props, monitor, component) {
    if (!monitor.getItem().id) return;

    const dragIndex = monitor.getItem().index;
    const hoverIndex = props.index;

    if (dragIndex === hoverIndex) return;

    const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 1.2;
    const clientOffset = monitor.getClientOffset();
    const hoverClientY = clientOffset.y - hoverBoundingRect.top;

    if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) return;
    if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) return;

    props.onReorder(props.index, monitor.getItem().id);
    monitor.getItem().index = hoverIndex;
  },

  drop(props, monitor, component) {
    if (!monitor.getItem().id) return props.onAdd(props.index);
  }
};


function collectSource(connectDnd, monitor) {
  return {
    connectDragSource: connectDnd.dragSource(),
    connectDragPreview: connectDnd.dragPreview(),
    isDragging: monitor.isDragging()
  };
}


function collectTarget(connectDnd, monitor) {
  return {
    connectDropTarget: connectDnd.dropTarget(),
    isOver: monitor.isOver(),
    item: monitor.getItem()
  };
}


export default connect()(
  DropTarget(dragTypes.SECTION, sectionTarget, collectTarget)(
    DragSource(dragTypes.SECTION, sectionSource, collectSource)(Section)
  )
);
