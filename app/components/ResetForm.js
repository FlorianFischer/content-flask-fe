import React from 'react';
import { PropTypes } from 'prop-types';
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react';
import FormError from './FormError';
import { Field, reduxForm } from 'redux-form';
import TextField from './TextField';
import { Link } from 'react-router-dom';
import messages from '../constants/messagesConstants';
import logo from '../assets/logo_color.svg';


const validate = values => {
  const errors = {};

  if (values.password && values.password.length < 8) {
    errors.password = 'Please enter at least 8 characters';
  }

  if (!values.password) {
    errors.password = 'Required';
  }

  return errors;
};


class ResetForm extends React.Component {
  render() {
    const {
      handleSubmit,
      success,
      submitting,
      invalid,
      tokenInValidMessage
    } = this.props;
    let { error } = this.props;
    error = error || tokenInValidMessage;

    return (
      <Grid
        textAlign="right"
        style={{ height: '100%' }}
        verticalAlign="middle"
        className="auth-container"
      >
        <Grid.Column>
          <Form className="auth-form" size="large" onSubmit={ handleSubmit }>
            <Image className="logo" src={ logo } />
            <Segment>
              {error &&
                <Message key={ error } negative>
                  <Message.Header>{ error }</Message.Header>
                </Message>
              }
              {success &&
                <div>
                  <Message key="reset-success" positive>
                    <Message.Header>{ messages.RESET_SUCCESS }</Message.Header>
                    <Link to="/login">Login now!</Link>
                  </Message>
                </div>
              }
              {(!error && !success) && (
                <div>
                  <Field
                    placeholder="8+ characters"
                    type="password"
                    name="password"
                    component={ TextField }
                  />
                  <div className="actions primary">
                    <Link className="passive" to="/login">Login</Link>
                    <Button
                      className="primary"
                      loading={ submitting }
                      disabled={ invalid }
                      size="large">
                      Reset password
                    </Button>
                  </div>
                </div>
              )}
            </Segment>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}


ResetForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.string,
  submitting: PropTypes.bool,
  success: PropTypes.bool,
  invalid: PropTypes.bool,
  tokenInValidMessage: PropTypes.string
};


export default reduxForm({
  form: 'resetForm',
  validate
})(ResetForm);
