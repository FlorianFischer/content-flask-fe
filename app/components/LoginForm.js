import React from 'react';
import { PropTypes } from 'prop-types';
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react';
import FormError from './FormError';
import { Field, reduxForm } from 'redux-form';
import TextField from './TextField';
import { Link } from 'react-router-dom';
import logo from '../assets/logo_color.svg';


const validate = values => {
  const errors = {};

  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email';
  }

  if (!values.email) {
    errors.email = 'Required';
  }

  if (!values.password) {
    errors.password = 'Required';
  }

  return errors;
};


class LoginForm extends React.Component {
  render() {
    const { error, handleSubmit, submitting, invalid } = this.props;

    return (
      <Grid
        textAlign="center"
        style={{ height: '100%' }}
        verticalAlign="middle"
      >
        <Grid.Column>
          <Form size="large" onSubmit={ handleSubmit }>
            <Image className="logo" src={ logo } />
            {error &&
              <Message key={ error } negative>
                <Message.Header>{ error }</Message.Header>
              </Message>
            }
            <Field
              icon="user"
              iconPosition="left"
              placeholder="Email"
              type="email"
              name="email"
              component={ TextField }
            />
            <Field
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              type="password"
              name="password"
              component={ TextField }
            />
            <div className="actions primary">
              <Link className="passive" to="/forgot">Forgot your password?</Link>
              <Button
                className="primary"
                loading={ submitting }
                size="large">
                Login
              </Button>
            </div>
            <div className="actions">
              <Link to="/signup">Create my ContentFlask account!</Link>
            </div>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}


LoginForm.propTypes = {
  handleSubmit: PropTypes.func,
  error: PropTypes.string,
  submitting: PropTypes.bool,
  invalid: PropTypes.bool
};


export default reduxForm({
  form: 'loginForm',
  validate
})(LoginForm);
