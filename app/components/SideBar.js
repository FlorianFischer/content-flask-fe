import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as authActions from '../actions/authActions';
import { bindActionCreators } from 'redux';
import logo from '../assets/logo_color.svg';
import { Image } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';


const SideBar = props => {
  return (
    <div className="sidebar">
      <div className="logo">
        <Image src={ logo } />
      </div>
      <div className="sidebar-main">
        <ul className="navigation">
          <NavLink to="/projects" exact activeClassName="active">Projects</NavLink>
        </ul>
        <span onClick={ props.actions.logout }>Logout</span>
      </div>
    </div>
  );
};


SideBar.propTypes = {
  actions: PropTypes.object,
  isAuthenticated: PropTypes.bool.isRequired
};


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch)
  };
}


function mapStateToProps(state, props) {
  return {
    isAuthenticated: state.session.isAuthenticated
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
