import React from 'react';
import { PropTypes } from 'prop-types';


const ProjectCard = props => {
  const progressPercent = props.progress ? Math.round(props.progress * 100) : 0;
  const progressClassName = `progress-circle progress-circle-${progressPercent}`;

  return (
    <div className={ 'c-card project-card ' +  props.state }
         onClick={ props.onClick }>
      <div className="circle">
        <div className={ progressClassName }>
          <div className="overlay">{ progressPercent }%</div>
        </div>
      </div>
      <span className="name">{ props.name }</span>
    </div>
  );
};


ProjectCard.propTypes = {
  name: PropTypes.string.isRequired,
  progress: PropTypes.number,
  state: PropTypes.string.isRequired,
  createdAt: PropTypes.number,
  onClick: PropTypes.func,
  deadline: PropTypes.object
};


export default ProjectCard;
