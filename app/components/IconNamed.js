import React from 'react';
import PropTypes from 'prop-types';
import IconBoxed from './IconBoxed';


const IconNamed = props => {
  return (
    <div className="icon named">
      <IconBoxed color={ props.color } name={ props.icon } size="large"/>
      <div className="text">
        <div className="title">{ props.title }</div>
        <div className="subtitle">{ props.subtitle }</div>
      </div>
    </div>
  );
};


IconNamed.propTypes = {
  color: PropTypes.string,
  icon: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,
};


export default IconNamed;
