import React from 'react';
import PropTypes from 'prop-types';
import { Confirm, Button, Modal } from 'semantic-ui-react';
import IconNamed from '../components/IconNamed';


class AsyncConfirm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: false };
  }


  // EVENTS


  handleConfirm() {
    if (!this.props.onConfirm) return this.props.onCancel();
    if (this.state.isLoading) return;
    this.setState({ isLoading: true });
    this.props.onConfirm().finally(() => this.setState({ isLoading: false }));
  }


  // RENDER


  render() {
    const { cancelText, noCancel, title, subtitle, confirmText, ...rest } = this.props;
    const confirmButton =
    (<Button
     loading={ this.state.isLoading }
     onClick={ this.handleConfirm.bind(this) }
     className="primary">{ confirmText }
    </Button>);

    let cancelButton =
    (<Button
     className="tertiary">Cancel
    </Button>);

    if (noCancel) cancelButton = null;

    const content =
    (
      <Modal.Content>
        <IconNamed
         icon="exclamation"
         color="red"
         title={ title }
         subtitle={ subtitle } />
      </Modal.Content>
    );

    return (
      <Confirm
       { ...rest }
       className="async-confirm"
       content={ content }
       onConfirm={ () => null }
       confirmButton={ confirmButton }
       cancelButton={ cancelButton } />
    );
  }
}


AsyncConfirm.propTypes = {
  confirmText: PropTypes.string,
  cancelText: PropTypes.string,
  onConfirm: PropTypes.func,
  onCancel: PropTypes.func.isRequired,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  noCancel: PropTypes.bool
};


export default AsyncConfirm;
