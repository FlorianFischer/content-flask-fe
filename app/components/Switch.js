import React from 'react';
import { PropTypes } from 'prop-types';
import { Button } from 'semantic-ui-react';


const Switch = props => {
  return (
   <div className="button-group or">
     <Button
      className={ `${props.isLeft ? 'primary' : 'flat'}` }
      onClick={ props.onLeftClick }>
      { props.leftContent }
     </Button>
     <Button
      className={ `${!props.isLeft ? 'primary' : 'flat'}` }
      onClick={ props.onRightClick }>
      { props.rightContent }
     </Button>
   </div>
 );
};


Switch.propTypes = {
  onLeftClick: PropTypes.func.isRequired,
  onRightClick: PropTypes.func.isRequired,
  isLeft: PropTypes.bool.isRequired,
  leftContent: PropTypes.string.isRequired,
  rightContent: PropTypes.string.isRequired
};


export default Switch;
