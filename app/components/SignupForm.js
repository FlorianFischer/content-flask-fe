import React from 'react';
import { PropTypes } from 'prop-types';
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react';
import FormError from './FormError';
import { Field, reduxForm } from 'redux-form';
import TextField from './TextField';
import { Link } from 'react-router-dom';
import logo from '../assets/logo_color.svg';


const validate = values => {
  const errors = {};

  ['firstName', 'lastName', 'company', 'email', 'password'].forEach(key => {
    if (!values[key]) errors[key] = 'Required';
  });

  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email';
  }

  if (!values.password) errors.password = 'Required';
  if (values.password && values.password.trim().length < 8) {
    errors.password = 'Please enter at least 8 characters';
  }

  return errors;
};


class SignupForm extends React.Component {
  render() {
    const { handleSubmit, submitting, invalid, error } = this.props;

    return (
      <Grid
        textAlign="center"
        style={{ height: '100%' }}
        verticalAlign="middle">
        <Grid.Column>
          <Form size="large" onSubmit={ handleSubmit }>
            <Image className="logo" src={ logo } />
            {error &&
              <Message key={ error } negative>
                <Message.Header>{ error }</Message.Header>
              </Message>
            }
            <Field
              placeholder="First Name"
              type="text"
              name="firstName"
              component={ TextField }/>
            <Field
              placeholder="Last Name"
              type="text"
              name="lastName"
              component={ TextField }/>
            <Field
              placeholder="Company"
              type="text"
              name="company"
              component={ TextField }/>
            <Field
              placeholder="you@company.com"
              type="email"
              name="email"
              component={ TextField }/>
            <Field
              placeholder="8+ characters"
              type="password"
              name="password"
              component={ TextField }/>
            <div className="actions primary">
              <Link className="passive" to="/login">Have an account already?</Link>
              <Button
                className="primary"
                disabled={ invalid }
                loading={ submitting }
                size="large">
                Signup
              </Button>
            </div>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}


SignupForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool,
  invalid: PropTypes.bool,
  error: PropTypes.string
};


export default reduxForm({
  form: 'signupForm',
  validate
})(SignupForm);
