import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import * as authActions from '../actions/authActions';


class PrivateRoute extends React.Component {
  componentDidMount() {
    // if (!this.props.isAuthenticated) this.props.logout();
  }


  render() {
    const { component: Component, isAuthenticated, ...rest } = this.props;

    return (<Route { ...rest } render={ props => (
      isAuthenticated
        ? <Component { ...props } />
        : <Redirect to="/login" />
    )} />);
  }
}


PrivateRoute.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  component: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired
};


function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(authActions.logout())
  };
}


function mapStateToProps(state, props) {
  return {
    isAuthenticated: state.session.isAuthenticated
  };
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PrivateRoute);
