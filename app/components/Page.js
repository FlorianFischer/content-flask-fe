import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import TextField from './TextField';
import { Form, Message, Grid, Button, Loader, Icon } from 'semantic-ui-react';
import TextArea from './TextArea';
import { connect } from 'react-redux';
import * as pageActions from '../actions/pageActions';
import { bindActionCreators } from 'redux';
import Checkbox from '../components/Checkbox';
import AsyncConfirm from '../components/AsyncConfirm';
import SectionList from '../components/SectionList';
import { getSectionsPerPage } from '../selectors/pageSelectors';


class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showConfirmDelete: false,
      showPageDetails: true
    };
  }


  // EVENTS


  handleUpdatePage(field, event, value) {
    this.props.updatePage(this.props.form, { [field]: value });
  }


  // RENDER


  render() {
    const { showPageDetails } = this.state;

    return (
      <div>
        <Form className="ui form page">
          <Grid>
            <Grid.Row>
              <Grid.Column width={ 16 }>
                <div className="form-item-heading">
                  <div className="headline circle-before">
                    <span>{ this.props.page.name }</span>
                    <span className="subtitle">Page details</span>
                  </div>
                  <div className="item-controls">
                    <Icon
                     name={ `chevron ${showPageDetails ? 'up' : 'down'}` }
                     size="small"
                     onClick={ () => this.setState({ showPageDetails: !showPageDetails }) } />
                    <Button
                     icon="trash"
                     size="small"
                     content="Delete"
                     className="secondary"
                     onClick={ () => this.setState({ showConfirmDelete: true }) } />
                    <AsyncConfirm
                     confirmText="Delete page"
                     title="Are you sure?"
                     subtitle="All progress on this page will be lost"
                     open={ this.state.showConfirmDelete }
                     onCancel={ () => this.setState({ showConfirmDelete: false }) }
                     onConfirm={ this.props.handleDeletePage.bind(this, this.props.page) } />
                  </div>
                </div>
              </Grid.Column>
            </Grid.Row>
            {showPageDetails &&
                <Grid.Row>
                  <Grid.Column width={ 10 }>
                    <Field
                     placeholder="Page name"
                     label="Page name"
                     name="name"
                     onBlur={ this.handleUpdatePage.bind(this, 'name') }
                     component={ TextField } />
                  </Grid.Column>
                  <Grid.Column width={ 2 }>
                    <Field
                      name="has_description"
                      label="Additional info"
                      onChange={ this.handleUpdatePage.bind(this, 'has_description') }
                      component={ Checkbox }/>
                  </Grid.Column>
                </Grid.Row>
              }
              {showPageDetails && this.props.page.has_description &&
                <Grid.Row>
                  <Grid.Column width={ 16 }>
                    <Field
                     placeholder="Describe this page"
                     label="Description"
                     name="description"
                     onBlur={ this.handleUpdatePage.bind(this, 'description') }
                     component={ TextArea } />
                  </Grid.Column>
                </Grid.Row>
              }
          </Grid>
        </Form>

        <SectionList
         pageId={ this.props.page._id || this.props.page._tempId }
         sections={ this.props.sections } />
      </div>
    );
  }
}


Page.propTypes = {
  updatePage: PropTypes.func,
  form: PropTypes.string.isRequired,
  initialValues: PropTypes.object,
  page: PropTypes.object.isRequired,
  projectId: PropTypes.string.isRequired,
  sections: PropTypes.array,
  handleDeletePage: PropTypes.func.isRequired
};


function mapStateToProps(state, props) {
  return {
    sections: getSectionsPerPage(props.page.sections)(state)
  };
}


function mapDispatchToProps(dispatch) {
  return {
    updatePage: (id, values) => dispatch(pageActions.updatePage(id, values))
  };
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(reduxForm()(Page));
