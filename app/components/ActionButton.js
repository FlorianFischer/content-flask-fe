import React from 'react';
import { PropTypes } from 'prop-types';
import { Icon, Loader } from 'semantic-ui-react';


class ActionButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: false };
    this._buttonIcon = this._buttonIcon.bind(this);
  }

  // INTERNAL


  _buttonIcon(icon, isMainButton) {
    if (isMainButton) {
      return this.state.isLoading
                     ? <Loader active inverted size="tiny"/>
                     : <Icon name={ icon } fitted/>;
    }

    return <Icon name={ icon } fitted/>;
  }


  // EVENTS


  handleClick(action, isAsync) {
    if (!isAsync) return this.props.action();
    if (this.state.isLoading) return;

    this.setState({ isLoading: true }, () => {
      action().finally(() => this.setState({ isLoading: false }));
    });
  }


  // RENDER


  render() {
    const { items } = this.props;

    return (
      <div className="floating-action-button">
        {items.map((item, index) => {
          const { action, label, icon, isAsync } = item;
          const isMainButton = index === 0;

          return (
            <div className={ `button ${ isMainButton ? 'main' : 'child' }` }
                 onClick={ this.handleClick.bind(this, action, isAsync) }
                 key={ index }>
              <div className="positioner">
                { this._buttonIcon(icon, isMainButton) }
              </div>
              {label && (
                <span className="label">{ label }</span>
              )}
            </div>
          );
        })}
      </div>
    );
  }
}


ActionButton.propTypes = {
  action: PropTypes.func.isRequired,
  label: PropTypes.string,
  items: PropTypes.array,
  isAsync: PropTypes.bool
};


export default ActionButton;
