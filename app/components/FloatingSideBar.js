import React from 'react';
import PropTypes from 'prop-types';
import sizeMe from 'react-sizeme';
import ElementList from '../components/ElementList';


class FloatingSideBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isOpen: false };
  }


  // LIFECYCLE


  componentDidMount() {
    document.body.addEventListener('mousemove', this.handleMouseMove.bind(this));
  }


  componentWillUnmount() {
    document.body.removeEventListener('mousemove', this.handleMouseMove.bind(this));
  }


  // EVENTS


  handleMouseMove(event) {
    const { isOpen } = this.state;
    if (isOpen && event.pageX < window.innerWidth - this.props.size.width) {
      return this.setState({ isOpen: false });
    }

    if (isOpen) return;
    if (event.pageX >= window.innerWidth - 20) {
      return this.setState({ isOpen: true });
    }
  }


  // RENDER


  render() {
    return (
      <div className={ `floating-side-bar ${this.state.isOpen ? 'is-open' : ''}` }>
        <ElementList />
      </div>
    );
  }
}

FloatingSideBar.propTypes = {
  size: PropTypes.shape({
    width: PropTypes.number.isRequired
  })
};


export default sizeMe()(FloatingSideBar);
