import React from 'react';
import { PropTypes } from 'prop-types';
import { Input as FormInput } from 'semantic-ui-react';
import defaultPropTypes from '../constants/defaultPropTypes';


const TextField = props => {
  const {
    input,
    label,
    type,
    icon,
    iconPosition,
    placeholder
  } = props;
  const { touched, error, warning } = props.meta;

  return (
    <div className={ `field ${label ? 'has-label' : ''}` }>
      {label && <label>{ label }</label>}
      <FormInput
        { ...input }
        fluid
        icon={ icon }
        iconPosition={ iconPosition }
        placeholder={ placeholder }
        type={ type }
        className="wrapped"
        key={ `${input.name}${props.meta.form}` }/>
      {(touched && error) && (
        <span className="form-error">
          <i className="icon remove"></i>
          <span className="error tooltip">{ error }</span>
        </span>
      )}
    </div>
  );
};


TextField.propTypes = defaultPropTypes;


export default TextField;
