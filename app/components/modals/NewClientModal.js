import React from 'react';
import PropTypes from 'prop-types';
import { Form, Message, Grid, Modal, Transition, Button } from 'semantic-ui-react';
import { Field, reduxForm } from 'redux-form';
import TextField from '../TextField';
import IconBoxed from '../IconBoxed';
import IconNamed from '../IconNamed';
import RemoteSubmitButton from '../RemoteSubmitButton';
import q from 'q';


const validate = values => {
  const errors = {};

  ['company_name', 'name', 'email'].forEach(key => {
    if (!values[key]) errors[key] = 'Required';
  });

  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email';
  }

  return errors;
};


class AddNewClientModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOpen: false
    };
  }


  _onClose() {
    this.props.reset();
    this.props.onClose();
  }


  // RENDER


  render() {
    const { props } = this;
    const { handleSubmit, submitting, invalid, error } = props;

    return (
      <Transition visible={ props.modalOpen } animation="fade down" duration="500">
        <Modal open={ props.modalOpen }
               onClose={ this._onClose.bind(this) }
               size="tiny">
          <Modal.Header>
            <IconNamed
             title="Add a new client"
             subtitle={ "We won't send an email" }
             icon="user"
             color="orange" />
          </Modal.Header>
          <Modal.Content>
            <Form onSubmit={ handleSubmit }>
              {error &&
                <Message key={ error } negative>
                  <Message.Header>{ error }</Message.Header>
                </Message>
              }
              <Field
                placeholder="Apple Inc."
                label="Client Company"
                type="text"
                name="company_name"
                component={ TextField }/>
              <Field
                placeholder="Tim Cook"
                type="text"
                label="Contact Name"
                name="name"
                component={ TextField }/>
              <Field
                placeholder="tim.cook@apple.com"
                label="Contact Email"
                type="email"
                name="email"
                component={ TextField }/>
            </Form>
          </Modal.Content>
          <Modal.Actions className="single">
            <RemoteSubmitButton
             formName="addNewClientForm"
             text="Create new client"
             loading={ submitting }
             disabled={ submitting }/>
          </Modal.Actions>
        </Modal>
      </Transition>
    );
  }
}


AddNewClientModal.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  onClose: PropTypes.func
};


export default reduxForm({
  form: 'addNewClientForm',
  validate
})(AddNewClientModal);
