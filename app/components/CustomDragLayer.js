import React from 'react';
import PropTypes from 'prop-types';
import SectionPreview from '../components/SectionPreview';
import { DragLayer } from 'react-dnd';
import * as dragTypes from '../constants/dragTypes';


const layerStyles = {
  position: 'fixed',
  pointerEvents: 'none',
  zIndex: 100,
  left: 0,
  top: 0,
  width: '100%',
  height: '100%',
};


function getItemStyles(props) {
  const { initialOffset, currentOffset } = props;
  if (!initialOffset || !currentOffset) return { display: 'none' };

  const { x, y } = currentOffset;
  const transform = `translate(${x}px, ${y}px)`;

  return {
    transform,
    WebkitTransform: transform,
  };
}


class CustomDragLayer extends React.Component {
  renderItem(type, item) {
    switch(type) {
      case dragTypes.SECTION:
        if (item.hasOwnProperty('index')) return null;
        return <SectionPreview name="New Section" />;
      default:
        return null;
    }
  }


  render() {
    const { isDragging, itemType, item } = this.props;
    if (!isDragging) return null;

    return (
      <div style={ layerStyles }>
        <div style={ getItemStyles(this.props) }>
          { this.renderItem(itemType, item) }
        </div>
      </div>
    );
  }
}


CustomDragLayer.propTypes = {
  item: PropTypes.object,
  itemType: PropTypes.string,
  initialOffset: PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
  }),
  currentOffset: PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
  }),
  isDragging: PropTypes.bool.isRequired
};


function collect(monitor) {
  return {
    item: monitor.getItem(),
    itemType: monitor.getItemType(),
    initialOffset: monitor.getInitialSourceClientOffset(),
    currentOffset: monitor.getSourceClientOffset(),
    isDragging: monitor.isDragging()
  };
}


export default DragLayer(collect)(CustomDragLayer);
