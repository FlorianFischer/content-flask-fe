import React from 'react';
import PropTypes from 'prop-types';
import 'react-dates/initialize';
import { SingleDatePicker } from 'react-dates';
import defaultPropTypes from '../constants/defaultPropTypes';
import moment from 'moment';


class SingleDatePickerWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDatePickerFocused: false,
      date: moment(props.meta.initial)
    };
  }


  // LIFECYCLE


  componentWillReceiveProps(nextProps) {
    this.setState({ date: moment(nextProps.input.value) });
  }


  // RENDER


  render() {
    const {
      input,
      label,
      type,
      icon,
      iconPosition,
      name,
      placeholder
    } = this.props;
    const { touched, error, warning, initial } = this.props.meta;

    return(
      <div className="field date-picker" key={ name }>
        {label && <label key={ name }>{ label }</label>}
        <SingleDatePicker
         date={ this.state.date }
         onDateChange={ input.onChange }
         focused={ this.state.isDatePickerFocused }
         onFocusChange={({ focused }) => this.setState({ isDatePickerFocused: focused })}
         noBorder
         showDefaultInputIcon />
      </div>
    );
  }
}


SingleDatePickerWrapper.propTypes = defaultPropTypes;


export default SingleDatePickerWrapper;
