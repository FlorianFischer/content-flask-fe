import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';


const IconBoxed = props => {
  return (
    <div className={ `icon-boxed ${props.color}` }>
      <div className="positioner">
        <Icon name={ props.name } size={ props.size }/>
      </div>
    </div>
  );
};


IconBoxed.propTypes = {
  color: PropTypes.string,
  name: PropTypes.string.isRequired,
  size: PropTypes.string.isRequired
};


export default IconBoxed;
