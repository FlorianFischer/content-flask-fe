import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';


class Base extends React.Component {
  render() {
    const redirectTo = this.props.isAuthenticated ? '/projects' : '/login';
    return (<Redirect to={ redirectTo }/>);
  }
}


Base.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};


function mapStateToProps(state) {
  return {
    isAuthenticated: state.session.isAuthenticated
  };
}


export default connect(mapStateToProps)(Base);
