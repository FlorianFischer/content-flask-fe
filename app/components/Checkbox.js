import React from 'react';
import { PropTypes } from 'prop-types';
import defaultPropTypes from '../constants/defaultPropTypes';
import { Checkbox } from 'semantic-ui-react';


const CheckboxWrapper = props => {
  const {
    input,
    label,
    type,
    icon,
    iconPosition,
    placeholder,
    toggle
  } = props;
  const { touched, error, warning } = props.meta;

  return (
    <div className={ `field checkbox ${label ? 'has-label' : ''}` }>
      {label && <label>{ label }</label>}
      <Checkbox
       onClick={ (event, data) => input.onChange(data.checked) }
       checked={ input.value }
       toggle={ toggle }/>
      {(touched && error) && (
        <span className="form-error">
          <i className="icon remove"></i>
          <span className="error tooltip">{ error }</span>
        </span>
      )}
    </div>
  );
};


CheckboxWrapper.propTypes = defaultPropTypes;


export default CheckboxWrapper;
