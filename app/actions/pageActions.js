import * as types from './types';
import errorMessages from '../constants/messagesConstants';
import RequestService from '../services/RequestService';
import q from 'q';
import messages from '../constants/messagesConstants';
import shortid from 'shortid';
import * as projectActions from '../actions/projectActions';


function createPageFailure() {
  return {
    type: types.CREATE_PAGE_FAILURE,
    error: errorMessages.CREATE_PAGE_FAILURE
  };
}


function deletePageFailure() {
  return {
    type: types.DELETE_PAGE_FAILURE,
    error: errorMessages.DELETE_PAGE_FAILURE
  };
}


export function addPage(page, projectId) {
  const sectionId = shortid.generate();
  const section = { name: 'New Section', _tempId: sectionId };

  page._tempId = shortid.generate();
  page.sections = [sectionId];

  return {
    type: types.ADD_PAGE,
    payload: { page, projectId, section }
  };
}


export function removePage(pageId, projectId) {
  return {
    type: types.REMOVE_PAGE,
    payload: { pageId, projectId }
  };
}


export function updatePage(id, values) {
  return {
    type: types.UPDATE_PAGE,
    payload: { id, values }
  };
}


export function createPage(projectId) {
  return dispatch => {
    return q.Promise((resolve, reject) => {
      RequestService.post(`/projects/${projectId}/pages`)
        .then(response => {
          if (!response.success) {
            dispatch(createPageFailure());
            reject();
            return;
          }

          dispatch({
            type: types.CREATE_PAGE_SUCCESS,
            page: response.payload,
            id: projectId
          });

          resolve(response.payload);
        })
        .catch(() => {
          dispatch(createPageFailure());
          reject();
        });
    });
  };
}


export function deletePage(projectId, pageId) {
  return dispatch => {
    return q.Promise((resolve, reject) => {
      RequestService.delete(`/projects/${projectId}/pages/${pageId}`)
        .then(response => {
          if (!response.success) {
            dispatch(deletePageFailure());
            reject();
            return;
          }

          dispatch({
            type: types.DELETE_PAGE_SUCCESS,
            payload: { projectId, pageId }
          });

          resolve();
        })
        .catch(() => {
          dispatch(deletePageFailure());
          reject();
        });
    });
  };
}
