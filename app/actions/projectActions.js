import * as types from './types';
import errorMessages from '../constants/messagesConstants';
import RequestService from '../services/RequestService';
import q from 'q';
import messages from '../constants/messagesConstants';
import { normalize, denormalize } from 'normalizr';
import { projectSchema, projectListSchema } from '../reducers/schemas';


export function createProjectFailure() {
  return {
    type: types.CREATE_PROJECT_FAILURE,
    error: 'Sorry, we could not create a new project. Please reload the page and try again.'
  };
}


export function fetchProjectsFailure() {
  return {
    type: types.FETCH_PROJECTS_FAILURE,
    error: 'Sorry, something went wrong fetching your projects. Please reload the page and try again.'
  };
}


export function fetchSingleProjectFailure() {
  return {
    type: types.FETCH_PROJECT_FAILURE,
    error: messages.FETCH_PROJECT_FAILURE
  };
}


export function updateProjectFailure() {
  return {
    type: types.SAVE_PROJECT_FAILURE,
    error: messages.SAVE_PROJECT_FAILURE
  };
}


export function deleteProjectFailure() {
  return {
    type: types.DELETE_PROJECT_FAILURE,
    error: messages.DELETE_PROJECT_FAILURE
  };
}


export function changeStateVisibility(filter) {
  return {
    type: types.CHANGE_STATE_VISIBILITY,
    filter
  };
}


export function changePage(page) {
  return {
    type: types.CHANGE_PAGE,
    page
  };
}


export function addClientToProject(client, id) {
  return {
    type: types.ADD_CLIENT_PROJECT,
    client,
    id
  };
}


export function setCurrentProject(id) {
  return {
    type: types.SET_CURRENT_PROJECT,
    id
  };
}


export function updateProject(id, values) {
  return {
    type: types.UPDATE_PROJECT,
    payload: { id, values }
  };
}


export function createProject() {
  return dispatch => {
    return q.Promise((resolve, reject) => {
      RequestService.post('/projects')
        .then(response => {
          if (!response.success) {
            dispatch(createProjectFailure());
            reject();
            return;
          }

          const { entities } = normalize(response.payload, projectSchema);

          dispatch({
            type: types.CREATE_PROJECT_SUCCESS,
            payload: entities
          });

          resolve(response.payload);
        })
        .catch(error => {
          dispatch(createProjectFailure());
          reject();
        });
    });
  };
}


export function getProjects() {
  return dispatch => {
    return q.Promise((resolve, reject) => {
      RequestService.get('/projects')
        .then(response => {
          if (!response.success) {
            dispatch(fetchProjectsFailure());
            reject();
            return;
          }

          const { entities } = normalize(response.payload, projectListSchema);

          dispatch({
            type: types.FETCH_PROJECTS_SUCCESS,
            payload: entities
          });

          resolve(response.payload);
        })
        .catch(error => {
          dispatch(fetchProjectsFailure());
          reject();
        });
    });
  };
}


export function getSingleProject(id) {
  return dispatch => {
    return q.Promise((resolve, reject) => {
      RequestService.get(`/projects/${id}`)
        .then(response => {
          if (!response.success) {
            dispatch(fetchSingleProjectFailure());
            reject();
          }

          const { entities } = normalize(response.payload, projectSchema);

          dispatch({
            type: types.FETCH_PROJECT_SUCCESS,
            payload: entities
          });

          resolve(response.payload);
        })
        .catch(error => {
          dispatch(fetchSingleProjectFailure());
          reject();
        });
    });
  };
}


export function saveProject(id) {
  return (dispatch, getState) => {
    const state = getState();
    const project = { ...state.projects.byId[id] };
    const denormalized = denormalize(
      { pages: project.pages },
      projectSchema,
      { pages: state.pages.byId, sections: state.sections.byId }
    );
    project.pages = denormalized.pages;

    return q.Promise((resolve, reject) => {
      RequestService.put(`/projects/${id}`, project)
        .then(response => {
          if (!response.success) {
            dispatch(updateProjectFailure());
            reject();
            return;
          }

          const { entities } = normalize(response.payload, projectSchema);

          dispatch({
            type: types.SAVE_PROJECT_SUCCESS,
            payload: entities
          });

          resolve(response.payload);
        })
        .catch(error => {
          dispatch(updateProjectFailure());
          reject();
        });
    });
  };
}


export function deleteProject(id) {
  return dispatch => {
    return q.Promise((resolve, reject) => {
      RequestService.delete(`/projects/${id}`)
        .then(response => {
          if (!response.success) {
            dispatch(deleteProjectFailure());
            reject();
            return;
          }

          dispatch({
            type: types.DELETE_PROJECT_SUCCESS,
            payload: { id }
          });

          resolve();
        })
        .catch(error => {
          dispatch(deleteProjectFailure());
          reject();
        });
    });
  };
}
