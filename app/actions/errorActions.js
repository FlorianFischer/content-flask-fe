import * as types from './types';


export function resetError() {
  return { type: types.RESET_ERROR };
}
