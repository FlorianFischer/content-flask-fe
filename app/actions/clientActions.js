import * as types from './types';
import errorMessages from '../constants/messagesConstants';
import RequestService from '../services/RequestService';
import q from 'q';
import messages from '../constants/messagesConstants';


function fetchClientsFailure() {
  return {
    type: types.FETCH_CLIENTS_FAILURE,
    error: messages.FETCH_CLIENTS_FAILURE
  };
}


export function createClient(data) {
  return dispatch => {
    return q.Promise((resolve, reject) => {
      RequestService.post('/clients', data)
        .then(response => {
          if (!response.success) {
            if (response.message === 'client-already-exists') {
              return reject(messages.CLIENT_ALREADY_EXISTS);
            }

            reject(messages.GENERAL_ERROR);
            return;
          }

          dispatch({
            type: types.CREATE_CLIENT_SUCCESS,
            client: response.payload
          });

          resolve(response.payload);
          return;
        })
        .catch(error => {
          reject(messages.GENERAL_ERROR);
        });
    });
  };
}


export function getClients() {
  return dispatch => {
    return q.Promise((resolve, reject) => {
      RequestService.get('/clients')
        .then(response => {
          if (!response.success) {
            dispatch(fetchClientsFailure());
            reject();
            return;
          }

          dispatch({
            type: types.FETCH_CLIENTS_SUCCESS,
            clients: response.payload
          });

          resolve(response.payload);
        })
        .catch(error => {
          dispatch(fetchClientsFailure());
          reject();
        });
    });
  };
}
