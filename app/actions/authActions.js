import * as types from './types';
import errorMessages from '../constants/messagesConstants';
import Auth from '../services/AuthService';
import q from 'q';
import RequestService from '../services/RequestService';


export function authSuccess(user) {
  return { type: types.AUTH_SUCCESS, user };
}


export function authFailure() {
  return { type: types.AUTH_FAILURE };
}


export function logout() {
  Auth.deauthenticateUser();
  return { type: types.LOGOUT };
}


export function login(email, password) {
  return dispatch => {
    return q.Promise((resolve, reject) => {
      RequestService.post('/login', { email, password })
        .then(response => {
          if (!response.success) {
            reject(response);
            return;
          }

          Auth.authenticateUser(response.payload.token);
          dispatch(authSuccess(response.payload.user));
          resolve();
        })
        .catch(reject);
    });
  };
}


export function signup(values) {
  return dispatch => {
    return q.Promise((resolve, reject) => {
      RequestService.post('/signup', values)
        .then(response => {
          if (!response.success) {
            reject(response);
            return;
          }

          Auth.authenticateUser(response.payload.token);
          dispatch(authSuccess(response.payload.user));
          resolve();
        })
        .catch(reject);
    });
  };
}


export function checkAuth() {
  return dispatch => {
    return q.Promise((resolve, reject) => {
      RequestService.get('/auth')
        .then(response => {
          if (!response.success) {
            Auth.deauthenticateUser();
            dispatch(authFailure());
            reject();
            return;
          }

          Auth.authenticateUser(response.payload.token);
          dispatch(authSuccess(response.payload.user));
          resolve();
        })
        .catch(reject);
    });
  };
}


export function requestReset(email) {
  return dispatch => {
    return q.Promise((resolve, reject) => {
      RequestService.post('/forgot', { email })
        .then(response => {
          if (!response.success) {
            reject();
            return;
          }

          resolve();
        })
        .catch(reject);
    });
  };
}


export function resetPassword(password, token) {
  return dispatch => {
    return q.Promise((resolve, reject) => {
      RequestService.post('/reset', { password, token })
        .then(response => {
          if (!response.success) {
            reject(response.message);
            return;
          }

          resolve();
        })
        .catch(reject);
    });
  };
}
