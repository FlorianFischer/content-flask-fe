import * as types from './types';
import shortid from 'shortid';


export function addSection(pageId, index) {
  const sectionId = shortid.generate();
  const section = { name: 'New Section', _tempId: sectionId };

  return {
    type: types.ADD_SECTION,
    payload: {
      section,
      pageId,
      index
    }
  };
}


export function updateSection(id, values) {
  return {
    type: types.UPDATE_SECTION,
    payload: { id, values }
  };
}


export function reorderSection(pageId, sectionId, index) {
  return {
    type: types.REORDER_SECTION,
    payload: { pageId, sectionId, index }
  };
}


export function removeSection(pageId, sectionId) {
  return {
    type: types.REMOVE_SECTION,
    payload: { pageId, sectionId }
  };
}
