import AuthService from './AuthService';


const BASE_URL = process.env.NODE_ENV === 'production'
                   ? 'http://content-api.edtjx8vvxk.eu-central-1.elasticbeanstalk.com/api'
                   : 'http://localhost:8080/api';

class RequestService {
  static post(url, data) {
    const headers = new Headers({
      'Content-Type': 'application/json'
    });

    if (AuthService.isUserAuthenticated()) {
      headers.set('Authorization', `Bearer ${AuthService.getToken()}`);
    }

    const request = new Request(`${BASE_URL}${url}`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers
    });

    return fetch(request)
      .then(response => response.json())
      .catch(error =>  error);
  }


  static get(url) {
    const headers = new Headers();

    if (AuthService.isUserAuthenticated()) {
      headers.set('Authorization', `Bearer ${AuthService.getToken()}`);
    }

    const request = new Request(`${BASE_URL}${url}`, {
      method: 'GET',
      headers
    });

    return fetch(request)
      .then(response => response.json());
  }


  static put(url, data) {
    const headers = new Headers({
      'Content-Type': 'application/json'
    });

    if (AuthService.isUserAuthenticated()) {
      headers.set('Authorization', `Bearer ${AuthService.getToken()}`);
    }

    const request = new Request(`${BASE_URL}${url}`, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers
    });

    return fetch(request)
      .then(response => response.json())
      .catch(error =>  error);
  }


  static delete(url) {
    const headers = new Headers();

    if (AuthService.isUserAuthenticated()) {
      headers.set('Authorization', `Bearer ${AuthService.getToken()}`);
    }

    const request = new Request(`${BASE_URL}${url}`, {
      method: 'DELETE',
      headers
    });

    return fetch(request)
      .then(response => response.json());
  }
}


export default RequestService;
