import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Base from './components/Base';
import LoginContainer from './containers/LoginContainer';
import ProjectsContainer from './containers/ProjectsContainer';
import ProjectDetailContainer from './containers/ProjectDetailContainer';
import SignupContainer from './containers/SignupContainer';
import ForgotContainer from './containers/ForgotContainer';
import ResetContainer from './containers/ResetContainer';
import PrivateRoute from './components/PrivateRoute';

export default (
	<Switch>
		<Route exact path="/" component={ Base } />
		<Route exact path="/login" component={ LoginContainer } />
		<Route exact path="/signup" component={ SignupContainer } />
		<Route exact path="/forgot" component={ ForgotContainer } />
		<Route exact path="/reset/:token" component={ ResetContainer } />
		<PrivateRoute exact path="/projects" component={ ProjectsContainer } />
		<PrivateRoute exact path="/projects/:id" component={ ProjectDetailContainer } />
	</Switch>
);
