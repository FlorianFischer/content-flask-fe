import { createSelector } from 'reselect';
import _ from 'lodash';


const getCurrentProjectId = state => state.projects.currentProject;
const getPages = state => state.pages.byId;
const getProjects = state => state.projects.byId;
const getSections = state => state.sections.byId;


export const getCurrentProject = createSelector(
  [getCurrentProjectId, getProjects],
  (id, projects) => {
    if (!id) return {};
    return projects[id];
  }
);


export const getSectionsPerPage = sectionIds => {
  return createSelector(
    [getSections],
    (sections) => sectionIds.map(id => sections[id]).filter(section => !!section)
  );
};


export const getCurrentPages = createSelector(
  [getCurrentProject, getPages],
  (project, pages) => {
    if (!project || !project.pages) return [];
    return project.pages.map(pageId => pages[pageId]);
  }
);
