import { createSelector } from 'reselect';
import _ from 'lodash';

const getFilter = state => state.alphabeticalFilter || 'ASC';
const getClients = state => Object.keys(state.byId).map(id => state.byId[id]);


export const getSortedClients = createSelector(
  [getFilter, getClients],
  (filter, clients) => {
    switch(filter) {
      case 'ASC':
        return _.orderBy(clients, ['company_name'], ['asc']);
      case 'DESC':
        return _.orderBy(clients, ['company_name'], ['desc']);
      default:
        return clients;
    }
  }
);


export const getSelectSortedClients = createSelector(
  [getSortedClients],
  (clients) => {
    return clients.map(client => {
      return { value: client._id, label: client.company_name };
    });
  }
);
