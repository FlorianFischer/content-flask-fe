import { createSelector } from 'reselect';


const getStateFilter = state => state.stateFilter;
const getPage = state => state.page;
const getProjectsPerPage = state => state.projectsPerPage;
const getProjects = state => Object.keys(state.byId).map(id => state.byId[id]);


export const getVisibleProjects = createSelector(
  [getStateFilter, getProjects],
  (stateFilter, projects) => {
    switch(stateFilter) {
      case 'ACTIVE':
        return projects.filter(project => project.state === 'active');
      case 'DRAFT':
        return projects.filter(project => project.state === 'draft');
      default:
        return projects;
    }
  }
);


export const getVisibleProjectsPaged = createSelector(
  [getPage, getVisibleProjects, getProjectsPerPage],
  (page, projects, projectsPerPage) => {
    if (projects.length < projectsPerPage) return projects;
    const startIndex = page * projectsPerPage - projectsPerPage;
    const endIndex = page * projectsPerPage;

    return projects.slice(startIndex, endIndex);
  }
);
